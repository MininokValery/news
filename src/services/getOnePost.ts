import axios, {AxiosResponse} from "axios";

const getOnePost = (id = ':id'): Promise<AxiosResponse> => {
    return axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
}
export default getOnePost;