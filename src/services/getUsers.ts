import axios, {AxiosResponse} from "axios";

const getUsers = (): Promise<AxiosResponse> => {
    return axios.get("https://jsonplaceholder.typicode.com/users");
}
export default getUsers;