import axios, {AxiosResponse} from "axios";

const getOneUser = (id = ':id'): Promise<AxiosResponse> => {
    return axios.get(`https://jsonplaceholder.typicode.com/users/${id}`);
}
export default getOneUser;