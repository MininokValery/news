import axios, {AxiosResponse} from "axios";

const getPosts = (): Promise<AxiosResponse> => {
    return axios.get("https://jsonplaceholder.typicode.com/posts");
}
export default getPosts;