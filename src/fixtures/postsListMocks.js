const postsListMocks = [
    {
        userId: 1,
        id: 1,
        title: 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
        body: 'meduza.com'
    },
    {
        userId: 1,
        id: 2,
        title: 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
        body: 'meduza.com'
    },
    {
        userId: 1,
        id: 3,
        title: 'Вsunt aut facere repellat provident occaecati excepturi optio reprehenderit',
        body: 'meduza.com'
    },
    {
        userId: 1,
        id: 4,
        title: 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
        body: 'meduza.com'
    },
    {
        userId: 1,
        id: 5,
        title: 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
        body: 'meduza.com'
    },
    {
        userId: 1,
        id: 6,
        title: 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
        body: 'meduza.com'
    },
    {
        userId: 1,
        id: 7,
        title: 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
        body: 'meduza.com'
    },
    {
        userId: 1,
        id: 8,
        title: 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
        body: 'meduza.com'
    },
    {
        userId: 1,
        id: 9,
        title: 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
        body: 'meduza.com'
    },
    {
        userId: 1,
        id: 10,
        title: 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
        body: 'meduza.com'
    },
]

export default postsListMocks;