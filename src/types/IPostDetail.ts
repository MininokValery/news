export interface IPostDetail {
    id: number;
    userId: number;
    title: string;
    body: string;
}