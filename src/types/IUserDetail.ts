export interface IUserDetail {
    id: number;
    name: string;
    username: string;
    phone: number;
    website: string;
    email: string;
}