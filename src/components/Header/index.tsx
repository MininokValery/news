import React from 'react';
import { NavLink } from 'react-router-dom';

import {routeMain as routeMainHome} from 'pages/Home';
import {routeMain as routeMainContacts} from 'pages/Contacts';
import {routeMain as routeMainNewsListPage} from 'pages/NewsListPage';
import {routeMain as routeMainPostListPage} from 'pages/PostListPage';
import {routeMain as routeMainUserListPage} from 'pages/UserListPage';

import './main.scss';

const Header = () => {
    return (
        <header className="header">
            <nav className='nav'>
                <NavLink to='/' className={() =>''}>
                    Logo
                </NavLink>
                <ul className='nav-list'>
                    <li className='nav-item'>
                        <NavLink to={routeMainHome()}>
                            Главная
                        </NavLink>
                    </li>
                    <li className='nav-item'>
                        <NavLink to={routeMainNewsListPage()}>
                            Новости
                        </NavLink>
                    </li>
                    <li className='nav-item'>
                        <NavLink to={routeMainContacts()}>
                            Контакты
                        </NavLink>
                    </li>
                    <li className='nav-item'>
                        <NavLink to={routeMainPostListPage()}>
                            Posts
                        </NavLink>
                    </li>
                    <li className='nav-item'>
                        <NavLink to={routeMainUserListPage()}>
                            Users
                        </NavLink>
                    </li>
                </ul>
            </nav>
        </header>
    );
}

export default Header;