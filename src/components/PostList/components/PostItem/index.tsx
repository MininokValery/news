import React from 'react';

import { NavLink } from 'react-router-dom';

import { routeMain as routePostDetailPage } from 'pages/PostDetailPage';

import { IPostDetail } from 'types/IPostDetail';

import './main.scss';

interface IPostItemParams {
    item: IPostDetail;
}

const PostItem: React.FC<IPostItemParams> = ({item}) => (
    <NavLink className='post-item' to={routePostDetailPage(item.id.toString())}>
        <div className='post-title'>{item.title}</div>
        <div className='post-body'>{item.body}</div>
    </NavLink>
)
export default PostItem;