import React from 'react';

import PostItem from './components/PostItem';

import { IPostDetail } from 'types/IPostDetail';

import './main.scss';

interface IPostsListParams {
    list: IPostDetail[];
}

const PostList: React.FC<IPostsListParams> = ({list}) => (
    <div className='post-list'>
        {list.map((post: IPostDetail) => (
            <PostItem key={post.id} item={post}/>
        ))}
    </div>
)

export default PostList;