import React from 'react';

import Header from 'components/Header';
import Main from 'components/Main';
import Footer from 'components/Footer';

import './main.scss';

const AppContent = () => {
    return (
        <div className="appContent">
            <Header/>
            <Main/>
            <Footer/>
        </div>
    );
}

export default AppContent;