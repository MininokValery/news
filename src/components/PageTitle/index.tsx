import React from "react";

import './main.scss';

interface IPageTitleParams {
    title: JSX.Element;
}

const PageTitle: React.FC<IPageTitleParams> = ({title}) => <div className="page-title">{title}</div>

export default PageTitle;