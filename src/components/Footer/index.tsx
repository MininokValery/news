import React from 'react';

import './main.scss';

const Footer = () => {
    return (
        <footer className="footer">
            <div className='footer-logo'>
                <p className='footer-title'>Новостник</p>
                <span className='small'>Single Page Application</span>
            </div>
            <p className='small'>Дипломный проект</p>
            <div className='footer-info'>
                <span className='small'>Made by</span>
                <p className='footer-title'>Мининок Валерий</p>
            </div>
        </footer>
    );
}

export default Footer;