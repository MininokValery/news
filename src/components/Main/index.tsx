import React from 'react';
import {Routes, Route} from 'react-router-dom';

import Home, {routeMain as routeMainHome} from 'pages/Home';
import Contacts, {routeMain as routeMainContacts} from 'pages/Contacts';
import NewsListPage, {routeMain as routeMainNewsListPage} from 'pages/NewsListPage';
import NewsDetailPage, {routeMain as routeMainNewsDetailPage} from 'pages/NewsDetailPage';
import PostListPage, {routeMain as routeMainPostListPage} from 'pages/PostListPage';
import PostDetailPage, {routeMain as routeMainPostDetailPage} from 'pages/PostDetailPage';
import UserListPage, {routeMain as routeMainUserListPage} from 'pages/UserListPage';
import UserDetailPage, {routeMain as routeMainUserDetailPage} from 'pages/UserDetailPage';

import './main.scss';

const Main = () => {
    return (
        <main className='main'>
            <Routes>
                <Route
                    path = '/'
                    element = {<h1 className='spa-title'>Single Page Aplication</h1>}
                />
                <Route
                    path = {routeMainHome()}
                    element = {<Home/>}
                />
                <Route
                    path = {routeMainContacts()}
                    element = {<Contacts/>}
                />
                <Route
                    path = {routeMainNewsListPage()}
                    element = {<NewsListPage/>}
                />
                <Route
                    path = {routeMainNewsDetailPage()}
                    element = {<NewsDetailPage/>}
                />
                <Route
                    path = {routeMainPostListPage()}
                    element = {<PostListPage/>}
                />
                <Route
                    path = {routeMainPostDetailPage()}
                    element = {<PostDetailPage/>}
                />
                <Route
                    path = {routeMainUserListPage()}
                    element = {<UserListPage/>}
                />
                <Route
                    path = {routeMainUserDetailPage()}
                    element = {<UserDetailPage/>}
                />
            </Routes>
        </main>
    );
}

export default Main;