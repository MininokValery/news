import React from 'react';

import { NavLink } from 'react-router-dom';

import { routeMain as routeNewsDetailPage } from 'pages/NewsDetailPage';

import DateView from 'components/DateView';

import { INewsDetail } from 'types/INewsDetail';

import './main.scss';

interface INewsItemParams {
    item: INewsDetail;
}

const NewsItem: React.FC<INewsItemParams> = ({item}) => (
    <NavLink className='news-item' to={routeNewsDetailPage(item._id)}>
        <div className='title'>{item.title}</div>
        <div className='item-wrapper'>
            <p className='source'>{item.clean_url}</p>
            <DateView value={item.published_date}/>
        </div>
    </NavLink>
)

export default NewsItem;