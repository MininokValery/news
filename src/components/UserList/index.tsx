import React from 'react';

import UserItem from './components/UserItem';

import { IUserDetail } from 'types/IUserDetail';

import './main.scss';

interface IUserListParams {
    list: IUserDetail[];
}

const UserList: React.FC<IUserListParams> = ({list}) => (
    <div className='user-list'>
        {list.map((user: IUserDetail) => (
            <UserItem key={user.id} item={user}/>
        ))}
    </div>
)

export default UserList;