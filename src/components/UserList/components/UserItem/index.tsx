import React from 'react';

import { NavLink } from 'react-router-dom';

import { routeMain as routeUserDetailPage } from 'pages/UserDetailPage';

import { IUserDetail } from 'types/IUserDetail';

import './main.scss';

interface IUserItemParams {
    item: IUserDetail;
}

const UserItem: React.FC<IUserItemParams> = ({item}) => (
    <NavLink className='user-item' to={routeUserDetailPage(item.id.toString())}>
        <div className='user-title'>{item.name}</div>
        <div className='user-body'>{item.phone}</div>
    </NavLink>
)
export default UserItem;