import React, { useEffect } from 'react';
import routeMain from './routes';

// import { useDispatch, useSelector } from 'react-redux';
import { useSelector } from 'react-redux';//====
import { loadNews } from 'store/news/actions';
import { selectList } from 'store/news/selectors';
import { useTypedDispatch } from 'store';//====

import PageTitle from 'components/PageTitle';
import NewsList from 'components/NewsList';

import './main.scss';

const Home = () => {
    // const dispatch  = useDispatch();
    const dispatch  = useTypedDispatch();//====
    const newsList = useSelector(selectList);

    useEffect(() => {
        dispatch(loadNews());
    }, [dispatch])

    return (
        <section className='home'>
            <PageTitle
                title={
                    <h2>
                        Всегда <br /> свежие <span>новости</span>
                    </h2>
                }
            />
            {newsList.length > 0 && <NewsList list={newsList.slice(0,6)}/>}
        </section>
    )
}

export {routeMain};

export default Home;