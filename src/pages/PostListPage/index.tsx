import React, {useState, useEffect} from 'react';
import routeMain from './routes';

import PostList from 'components/PostList';

import getPosts from 'services/getPosts';

import { IPostDetail } from 'types/IPostDetail';

import './main.scss';

const PostListPage = () => {
    const [postList, setPostList] = useState<IPostDetail[]>([]);

    useEffect(() => {
        getPosts().then(response => {
            setPostList(response.data)
        })
    }, [])

    return (
        <section className='post-section'>
            {postList.length > 0 && <PostList list={postList}/>}
        </section>
    )
}

export {routeMain};

export default PostListPage;