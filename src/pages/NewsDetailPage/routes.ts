const routeMain = (id = ':id') => `/NewsDetailPage/${id}`;

export default routeMain;