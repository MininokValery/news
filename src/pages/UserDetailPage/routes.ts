const routeMain = (id = ':id') => `/UserDetailPage/${id}`;

export default routeMain;