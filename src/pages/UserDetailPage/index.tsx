import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import routeMain from './routes';

import getOneUser from 'services/getOneUser';

import { ID } from 'types/ID';
import { IUserDetail } from 'types/IUserDetail';

import './main.scss';

const UserDetailPage = () => {
    const {id} = useParams<ID>();
    const [user, setUser] = useState<IUserDetail | null>(null);

    useEffect(() => {
        getOneUser(id).then(response => {
            setUser(response.data);
        })
    }, [id])

    return (
        <section className='user-detail'>
            <h2>User Page</h2>
            {user ? (
                <div className='user-detail-wrapper'>
                    <h2 className='user-detail-title'>{user.username}</h2>
                    <p className='user-detail-body'>{user.phone}</p>
                    <p className='user-detail-body'>{user.email}</p>
                    <p className='user-detail-body'>{user.website}</p>
                </div>
            ): <></>}
        </section>
    )
}

export {routeMain};

export default UserDetailPage;