import React, {useState, useEffect} from 'react';
import routeMain from './routes';

import UserList from 'components/UserList';

import getUsers from 'services/getUsers';

import { IUserDetail } from 'types/IUserDetail';

import './main.scss';

const UserListPage = () => {
    const [userList, setUserList] = useState<IUserDetail[]>([]);

    useEffect(() => {
        getUsers().then(response => {
            setUserList(response.data)
        })
    }, [])

    return (
        <section className='user-section'>
            {userList.length > 0 && <UserList list={userList}/>}
        </section>
    )
}

export {routeMain};

export default UserListPage;