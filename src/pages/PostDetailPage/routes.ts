const routeMain = (id = ':id') => `/PostDetailPage/${id}`;

export default routeMain;