import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import routeMain from './routes';

import getOnePost from 'services/getOnePost';

import { ID } from 'types/ID';
import { IPostDetail } from 'types/IPostDetail';

import './main.scss';

const PostDetailPage = () => {
    const {id} = useParams<ID>();
    const [post, setPost] = useState<IPostDetail | null>(null);

    useEffect(() => {
        getOnePost(id).then(response => {
            setPost(response.data);
        })
    }, [id])

    return (
        <section className='post-detail'>
            <h2>Detail Page</h2>
            {post ? (
                <div className='post-detail-wrapper'>
                    <h2 className='post-detail-title'>{post.title}</h2>
                    <p className='post-detail-body'>{post.body}</p>
                </div>
            ): <></>}
        </section>
    )
}

export {routeMain};

export default PostDetailPage;