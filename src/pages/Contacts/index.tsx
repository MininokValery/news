import React, {useState} from 'react';
import routeMain from './routes';

import Rectangle from 'assets/img/Rectangle.jpg';

import Button from 'components/Button';
import Input from 'components/Input';

import './main.scss';

const Contacts = () => {

    const [title, setTitle] = useState<string>('');

    const handleChange = () => console.log('Click');

    const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setTitle(event.target.value);
    }

    return (
        <section className='contacts'>
            <div className='contacts-info'>
                <div className='contacts-phone'>
                    <a href='tel:+79991234576'>+7 (913) 401 56 00</a>
                </div>
                <div className='contacts-name'>
                    <p>
                        Валерий
                    </p>
                    <p>
                        Мининок
                    </p>
                </div>
                <div className='contacts-email'>
                    <a href='mailto:Minin-Ok@yandex.ru'>Minin-Ok@yandex.ru</a>
                </div>
                <p className='position'>
                    FrontEnd Developer
                </p>
                <p className='technologies'>
                    HTML CSS JS REACT
                </p>
            </div>
            <div className='contacts-image'>
                <img src={Rectangle} alt={Rectangle}/>
                <Button handleChange={handleChange} title='Click Me'/>
                <Input onChange={onChange} placeholder='введите текст' />
                <span>{title}</span>
            </div>
        </section>
    )
}

export {routeMain};

export default Contacts;